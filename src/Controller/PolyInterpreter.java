package Controller;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;

import Model.Polinom;
import View.Panel;

public class PolyInterpreter implements ActionListener {
	Panel panel;
	String[] monoame1;
	String[] monoame2;
	
	//We are dependent of the values on the panel
	public PolyInterpreter(Panel panel) {
		this.panel = panel;
	}
	
    public void actionPerformed(ActionEvent e) {
        String command = e.getActionCommand();
        if(panel.value.getText().isEmpty())
        	panel.x=0;
        else
        	panel.x=Integer.parseInt(panel.value.getText());
        
        if(command.equals("ok1"))  {
            String buffer = panel.polinom.getText();
        	Polinom poli = this.createPoli(buffer); 		
        	panel.controlLabel.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Value pol1: <b>" + poli.value() + "</b></p></html>");
        	panel.statusLabel.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Form Pol1: <b>" + poli.display() + "</b></p></html>");
        } else if(command.equals("ok2")) {
            String buffer = panel.polinom2.getText();
        	Polinom poli =  this.createPoli(buffer);
        	panel.controlLabel2.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Value pol2: <b>" + poli.value() + "</b></p></html>");
        	panel.statusLabel2.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Form Pol2: <b>" + poli.display() + "</b></p></html>");
        } else if(command.equals("plus")) {
            String buffer1 = panel.polinom.getText();
            String buffer2 = panel.polinom2.getText();
        	Polinom poli3 = this.add(buffer1, buffer2);
        	panel.controlLabel3.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Result(with value): <b>"+ poli3.value() + "</b></p></html>");
        	panel.statusLabel3.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Result form->ADD: <b>" + poli3.display() + "</b></p></html>");
        } else if(command.equals("minus")) {
            String buffer1 = panel.polinom.getText();
            String buffer2 = panel.polinom2.getText();
        	Polinom poli3 = this.sub(buffer1, buffer2);
        	panel.controlLabel4.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Result(with value): <b>"+ poli3.value() + "</b></p></html>");
        	panel.statusLabel4.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Result form->SUB: <b>" + poli3.display() + "</b></p></html>");
        } else if(command.equals("times")) {
            String buffer1 = panel.polinom.getText();
            String buffer2 = panel.polinom2.getText();
        	Polinom poli3 = this.times(buffer1, buffer2);
        	panel.controlLabel5.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Result(with value): <b>"+ poli3.value() + "</b></p></html>");
        	panel.statusLabel5.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Result form->MUL: <b>" + poli3.display() + "</b></p></html>");
        } else if(command.equals("divide")) {
        	String buffer1 = panel.polinom.getText();
            String buffer2 = panel.polinom2.getText();
            ArrayList<Polinom> polis = new ArrayList<Polinom>();
            polis = this.divide(buffer1, buffer2);
            panel.controlLabel6.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Result(with value): <b>" + polis.get(0).value() + "   |   " + polis.get(1).value() + "</b></p></html>");
            panel.statusLabel6.setText("<html><p style='font-family:Arial;font-weight:100;line-height:15px;'>Result form->DIV: <b>" + polis.get(0).display() + "  |   " + polis.get(1).display() + "</b></p></html>");
         }
    }
    
    private Polinom createPoli(String buffer) {
    	String monoame[];
    	String finalBuffer;
    	// We first verify if we must perform a derivation or an integration
    	if(buffer.toLowerCase().contains("deriv")) {
    		// We want to derivate
    		buffer.trim().replace("deriv", "");   
    		String actualBuffer = (buffer.contains("-")) ? buffer.replace("-", "+-") : buffer;
        	finalBuffer = (actualBuffer.charAt(0) == '+') ? actualBuffer.substring(1) : actualBuffer;
        	monoame = finalBuffer.split("\\+"); 
        	Polinom poli = new Polinom(monoame, panel.x);
        	poli.deriv();
        	return poli;
    	} else if(buffer.toLowerCase().contains("int")) {
    		// we want to integrate
    		buffer.replace("int", "").trim();   
    		String actualBuffer = (buffer.contains("-")) ? buffer.replace("-", "+-") : buffer;
        	finalBuffer = (actualBuffer.charAt(0) == '+') ? actualBuffer.substring(1) : actualBuffer;
        	monoame = finalBuffer.split("\\+"); 
        	Polinom poli = new Polinom(monoame, panel.x);
        	poli.integ();
        	return poli;
    	} else {//we just write the polynomials and perfom other operations
    		String actualBuffer = (buffer.contains("-")) ? buffer.replace("-", "+-") : buffer;
        	finalBuffer = (actualBuffer.charAt(0) == '+') ? actualBuffer.substring(1) : actualBuffer;
        	monoame = finalBuffer.split("\\+"); 
        	Polinom poli = new Polinom(monoame, panel.x);
        	return poli;
    	}
    }
    
    private Polinom add(String buffer1, String buffer2) {
    	Polinom poli1, poli2, poli3;
    	poli1 = createPoli(buffer1);
    	poli2 = createPoli(buffer2);
    	poli3 = poli1.add(poli2);
    	return poli3;
    }
    
    private Polinom sub(String buffer1, String buffer2) {
    	Polinom poli1, poli2, poli3;
    	poli1 = createPoli(buffer1);
    	poli2 = createPoli(buffer2);
    	poli3 = poli1.sub(poli2);
    	return poli3;
    }
    
    private Polinom times(String buffer1, String buffer2) {
    	Polinom poli1, poli2, poli3;
    	poli1 = createPoli(buffer1);
    	poli2 = createPoli(buffer2);
    	poli3 = poli1.times(poli2);
    	return poli3;
    }
    
    private ArrayList<Polinom> divide(String buffer1, String buffer2) {
    	Polinom poli1, poli2;
    	ArrayList<Polinom> polis = new ArrayList<Polinom>();
    	poli1 = createPoli(buffer1);
    	poli2 = createPoli(buffer2);
    	polis = poli1.divide(poli2);
    	return polis;
    }
	    
}