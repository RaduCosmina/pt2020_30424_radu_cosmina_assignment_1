package Model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class test5_deriv {

	@Test
	void test() {
		String[] m= {"2x", "x^2", "2x^3", "-3x^4"};
		Polinom p=new Polinom(m ,2);
		assertEquals(-24, p.value());
		p.deriv();
		assertEquals("2.0+2.0x+6.0x^2-12.0x^3", p.display());
		
		
	}

}
