package Model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class test1_add {

	@Test
	void test() {
		String[] monomStr1= {"x","x^2"};
		Polinom pol1=new Polinom(monomStr1,2);
		String[] monomStr2= {"2x^2","x", "2"};
		Polinom pol2=new Polinom(monomStr2,2);
		assertEquals(6.0, pol1.value());
		assertEquals(12.0, pol2.value());
		assertEquals("2.0x+3.0x^2+2.0",pol1.add(pol2).display());
	}

}
