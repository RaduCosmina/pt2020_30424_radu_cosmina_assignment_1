package Model;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;

import org.junit.jupiter.api.Test;

class test4_div {

	@Test
	void test() {

		ArrayList<Polinom> polis = new ArrayList<Polinom>();
		String[] m1= {"6x^2","2x"};
		Polinom pol1=new Polinom(m1,2);
		String[] m2= {"3x", "1"};
		Polinom pol2=new Polinom(m2,2);
		polis=pol1.divide(pol2);
		assertEquals("2.0x+0+0", polis.get(0).display() );
		assertEquals("0", polis.get(1).display() );
		
	}

}
