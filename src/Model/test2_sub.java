package Model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class test2_sub {

	@Test
	void test() {
		String[] monoStr1= { "2x", "3x^4", "x^2"};
		Polinom pol1=new Polinom(monoStr1, 2);
		assertEquals(56, pol1.value());
		String[] monoStr2= {"3x", "2x^2", "2"};
		Polinom pol2=new Polinom(monoStr2, 2);
		assertEquals(16,pol2.value());
		assertEquals("-x^1-x^2+3.0x^4-2.0",pol1.sub(pol2).display());
				
	}

}
