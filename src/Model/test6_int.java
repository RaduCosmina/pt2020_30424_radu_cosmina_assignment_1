package Model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class test6_int {

	@Test
	void test() {
		String[] m= {"-3x^2", "2x", "4"};
		Polinom p=new Polinom(m ,2);
		assertEquals(-4, p.value());
		p.integ();
		assertEquals("4.0x+x^2-x^3", p.display());
	}

}
