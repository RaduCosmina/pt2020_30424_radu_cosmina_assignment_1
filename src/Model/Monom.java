package Model;

import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Monom  {
	public String monom;
	private int power;
	private int coeff;
	public double dcoef;
	

	public Monom(String monom) {
		
		if(validate(monom))  {
			this.monom = monom;
		}
		dcoef = (double)coeff;
	}
	
	public Monom(int coeff, int power) {
		
		this.power = power;
		this.coeff = coeff;
		dcoef = coeff;
		this.monom = format();
	}
	
	public Monom(double dcoef, int coeff, int power) {
		
		this.power = power;
		this.coeff = coeff;
		this.dcoef = dcoef;
		this.monom = format();
	}
	
	private String format() {
		String container = new String("");
		if(this.dcoef==1)
			container= new String("x^" + this.power);
		if(this.dcoef==0)
			container=new String("0");
		if(this.power==1)
			container=new String(this.dcoef+"x");
		if(this.power==0)
			container=new String("1");
		if(this.dcoef!=0 && this.dcoef!=1 && this.power!=0 && this.dcoef!=1)
			container=new String(this.dcoef+"x^"+this.power);
		return container;
	}
	
	public void deriv() {
		if (power != 0) {
			dcoef = dcoef * power;
			coeff = (int) dcoef;
			power-=1;
		}
	}
	
	public void integ() {
		power++;
		dcoef = dcoef / power;
		coeff = (int)dcoef;
	}
	
	public boolean isNegative() {
		return (this.coeff < 0) ? true : false;
	}
	
	public String getMonom() {
		String cont=new String("");
		if(power==0)
			cont= Double.toString(dcoef);
		if(power==1)
			cont= (dcoef+"x");
		if(dcoef==0)
			cont= ("0");
		if(dcoef==1)
			cont= ("x^"+power);
		if(dcoef==-1)
			cont= ("-x^"+power);
		if(dcoef!=-1 && dcoef!=0 && dcoef!=1 && power!=0 && power!=1)
			cont= (dcoef+ "x^" +power);
		return cont;
	}//the form of each monomial that we send to Polinom
	
	public void display() {
		System.out.println("Power: " + power + " coefficient: " + coeff + "\n");
	}//just a function to verify the reading
	
	public int getCoeff() {
		return this.coeff;
	}
	
	public int getPower() {
		return this.power;
	}
	
	public int value(int x) {
		return (int) (dcoef * Math.pow(x, power));
	}//integer value
	
	public double dValue(double x) {
		return dcoef * Math.pow(x, power);
	}
	
	public void addCoef(int x) {
		this.coeff += x;
		this.dcoef = (double)coeff;
	}//a function to add the coeficients of those monomials with the same power
	
	public Monom times(Monom mon1) {
		Monom ret;
		double c = this.dcoef * mon1.dcoef;
		int d = (int)c;
		int p = this.getPower() + mon1.getPower();
		ret = new Monom(c,d,p);
		return ret;
	}//the multiplication of 2 monomials-used further in Polinom
	
	public Monom divide(Monom divizor) {
		Monom ret;
		double c = this.dcoef / divizor.dcoef;
		int d = (int)c;
		int p = this.getPower() - divizor.getPower();
		ret = new Monom(c, d, p);
		return ret;
	}//division of coefficients, used further in Polinom
	
	public static Comparator<Monom> getCompBypower() {   
		Comparator<Monom> comp = new Comparator<Monom>(){
		    @Override
		    public int compare(Monom s1, Monom s2) {
		    	return Integer.compare(s1.power, s2.power);
		    }        
		};
		return comp;
	}  //comparison of power
	
	private boolean validate(String monom) {
		 String[] buffer;
		// we verify if there are weird symbols except ^
		if(!monom.matches("^[a-zA-Z0-9\\^\\*\\- ]*")) return false;
		Pattern polyFormat = Pattern.compile("\\^");
		Matcher m = polyFormat.matcher(monom);
		String s = new String();
		while(m.find()) { s = m.group();}
		if(s.isEmpty()) {
			// doesn't contain ^ =>
			buffer = monom.split("[a-zA-Z]");
			if(buffer.length == 0) {
				coeff = 1;
				power = 1;
			} else {
				coeff = (!buffer[0].isEmpty()) ? Integer.parseInt(buffer[0]) : 1;
				power = (buffer[0] == monom) ? 0 : 1;
			}
		} else {
			// contains ^ =>
			buffer = monom.split("\\^"); 
			try {
				String nrStr = new String();
				for(int i = 0; i < buffer[0].length(); i++){
			        char c = buffer[0].charAt(i);
			        if(c==45) nrStr += c;
			        if(c > 47 && c < 58)nrStr += c;
			    }
				coeff = (nrStr.isEmpty()) ? 1 : Integer.parseInt(nrStr);
				power = Integer.parseInt(buffer[1]);
			} catch(NumberFormatException e) { System.out.println("Invalid format");}		
		} 
		return true;
	}//validation function

	void setCoeff(int i) {
		this.coeff=i;
	}

	
}