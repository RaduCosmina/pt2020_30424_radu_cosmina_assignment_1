package Model;



import java.util.*; 
public class Polinom {
	String[] monomStr;
	ArrayList<Monom> monom = new ArrayList<Monom>();
	public int x;
	
	public Polinom(String[] monomStr, int x) {
		this.monomStr = monomStr;
		this.x = x;
		for(int i=0; i<monomStr.length; i++) {
			System.out.println(monomStr[i]);
			monom.add(new Monom(monomStr[i]));
		}
		for (int i = 0; i < monom.size(); i++) {
		    Monom temp = monom.get(i);
		    temp.display();
		}
		// At the end we sort the polynomials
		this.sort_minify();
		this.sort_minify();
	}
	
	private Polinom(int x) {
		this.x = x;
	}
	
	private Polinom(Monom m, int x) {
		this.x = x;
		this.monom.add(m);
	}
	
	private void clean() {
		for(int i=0;i<monom.size();i++) 
		{
			if(monom.get(i).getCoeff() == 0)
				monom.remove(i);
		}
	}//
	
	public String display() {
		String display_pol = new String("");
		int i=0;
		while(i<monom.size())
		{
			if(monom.get(i).getCoeff() < 0) 
			{
				display_pol += monom.get(i).getMonom();
			}
			else 
			{
				
				if(i==0)
					display_pol+=monom.get(i).getMonom();
				else
					display_pol+="+" +monom.get(i).getMonom();
			}
			i++;
		}
		return display_pol;
	}
	
	public double value() {
		double s = 0;
		int i=0;
		while(i<monom.size())
		{
			s+=monom.get(i).value(x);
			i++;
		}
		return s;
	}//method to calculate the value of a polynomial for a given x
	
	public int getGrad() {
		int c=-1; //there is no monomial of negative degree
		int i=0;
		while(i<monom.size())
		{
			if(c < monom.get(i).getPower())
				c = monom.get(i).getPower();
			i++;
		}
		return c;
	}
	
	private void sort_minify() {
		int i=0;
		int j=0;
		while(i<monom.size())
		{
			int power = monom.get(i).getPower();
			j=i+1;
			while(j<monom.size())
			{
				int temp_power = monom.get(j).getPower();
				if(power == temp_power)
				{
					monom.get(i).addCoef(monom.get(j).getCoeff());
					this.monom.remove(j);
				}
				j++;
			}
			i++;
		}
		Collections.sort(this.monom, Monom.getCompBypower());
	}//if we have more monomials of the same degree in the same polynomial, we just add their coefficients
	
	public void deriv() {
		int i=0;
		while(i<monom.size()){
			this.monom.get(i).deriv();
			i++;
		}
	}//the derivation method
	
	public void integ() {
		int i=0;
		while(i<monom.size()) {
			this.monom.get(i).integ();
			i++;
		}
	}//the integration method
	
	public Polinom add(Polinom poli2) {
		// poli3=the resulting polynomial
		Polinom poli3 = new Polinom(x);
		int i=0;
		while(i<this.monom.size())
		{
			// by now, the polynomials are sorted and minimized
			int p1 = this.monom.get(i).getPower();
			int c1 = this.monom.get(i).getCoeff();
			
			int i_pow = poli2.getIndexpower(p1);
			if(i_pow == -1) {
				// didn't find any so we just add a new monomial
				poli3.monom.add(new Monom(c1, p1));
			} else {
				// found something in poli2 so we add
				int p3 = poli2.monom.get(i_pow).getPower();
				int c3 = poli2.monom.get(i_pow).getCoeff();
				poli3.monom.add(new Monom(c1+c3, p3));
				// eliminate what we already have
				poli2.monom.remove(i_pow);
			}
			i++;
		}
		// now just add what is left in poli2
		int j=0;
		while(j<poli2.monom.size())
		{
			int p2 = poli2.monom.get(j).getPower();
			int c2 = poli2.monom.get(j).getCoeff();
			poli3.monom.add(new Monom(c2, p2));
			j++;
		}
		return poli3;
	}
	
	public Polinom times(Polinom poli2) {
		Polinom poli3 = new Polinom(x);
		//poli3=the resulting polynomial
		int i=0;
		int j=0;
		while(i<this.monom.size()) {//for every monomial in the polynomial
			Monom m1 = this.monom.get(i);
			while(j<poli2.monom.size())
			 {
				Monom m2 = poli2.monom.get(j);
				Monom m3 = m1.times(m2);//multiply with every monomial in poli2
				// to simplify the process, we just add the monomials
				poli3.monom.add(m3);
				j++;
			}i++;
		}
		poli3.sort_minify();
		poli3.sort_minify();
		return poli3;
	}
	
	public Polinom sub(Polinom poli2) {
		Polinom poli3=new Polinom(x);
		int i=0;
		while(i<poli2.monom.size()) {
			int c=poli2.monom.get(i).getCoeff() *(-1);//just reverse the sign of each monomial
			poli2.monom.get(i).setCoeff(c);
			i++;
		}
		poli3 = this.add(poli2);//perform an ususal addition
		poli3.clean();
		return poli3;
	} 
	
	public ArrayList<Polinom> divide(Polinom divisor) {
		ArrayList<Polinom> polis = new ArrayList<Polinom>();//array of polynomials to hold the quotient and the reminder
		Polinom quotient = new Polinom(x);
		Polinom reminder = new Polinom(x);
		reminder = this;
		if(reminder.getGrad() < divisor.getGrad()) {
			quotient.monom.add(new Monom(0,0));
		} else {
			while(reminder.getGrad() >= divisor.getGrad()) {
				Monom dm = cautaMaxim(reminder);
				Monom im = cautaMaxim(divisor);
				Monom m = dm.divide(im);
				Polinom pm = new Polinom(m, x);
				quotient.monom.add(m);
				reminder = reminder.sub(divisor.times(pm));
			}
		}
		if(reminder.monom.isEmpty()) reminder.monom.add(new Monom(0,0));
		polis.add(quotient);
		polis.add(reminder);
		return polis;
	}//algorithm of a long division of polynomials
	
	private Monom cautaMaxim(Polinom pol) {
		int power = pol.getGrad();
		int iP = pol.getIndexpower(power);
		return pol.monom.get(iP);
	}
	
	private int getIndexpower(int power) {
		int i=0;
		while(i<monom.size()) {
			Monom mon = monom.get(i);
			if(power == mon.getPower()) 
				return i;
			i++;
		}
		return -1;
	}

}