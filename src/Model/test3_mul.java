package Model;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class test3_mul {

	@Test
	void test() {
		String[] m1= {"x", "x^2"};
		Polinom pol1=new Polinom(m1,2);
		String[] m2= {"2", "x", "-2x^3"};
		Polinom pol2=new Polinom(m2,2);
		assertEquals(6, pol1.value());
		assertEquals(-12, pol2.value());
		assertEquals("2.0x+3.0x^2+x^3-2.0x^4-2.0x^5", pol1.times(pol2).display());
	}

}
